# Human-Protien-Atlas-Image-Classification

- This is a try to Compete in this Competition: https://www.kaggle.com/c/human-protein-atlas-image-classification

# Kaggle
- you can use: https://github.com/Kaggle/kaggle-api (official api) for submission and downloading of data
- or you can use: https://github.com/reshamas/fastai_deeplearn_part1/blob/master/tools/download_data_kaggle_cli.md (it can resume uncompleted downloaded files - dataset files, and it faster in downloading)
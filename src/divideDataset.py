#python packages
import sys
import logging
import logging.handlers
import os
import functools

#other packages
import pandas as pd
import numpy as np



def initializeLogger(logger_name, logger_file_path = '../application.log'):
    #ensure that the proper file name is used
    if not logger_file_path.endswith('.log'):
        logger_file_path += '.log'
    all_logs_file_path = logger_file_path.replace('.log', '_all.log')
    last_run_file_path = logger_file_path.replace('.log', '_lastrun.log')
    logging_format = '[%(asctime)s] [%(threadName)-12.12s] %(name)-12s %(levelname)-8s %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    std_error_handler = logging.StreamHandler(sys.stderr)
    std_error_handler.setLevel(logging.ERROR)
    logging.basicConfig(level=logging.DEBUG,
                        format=logging_format,
                        datefmt=date_format,
                        handlers=[
                            logging.handlers.RotatingFileHandler(all_logs_file_path, maxBytes=1024*1024, backupCount=10, mode='a'), #output logger file which contains all logs
                            logging.FileHandler(last_run_file_path, mode='w'),  # output logger file which contain only the last run log
                            logging.StreamHandler(sys.stdout), #output to console
                            std_error_handler #output of errors
                        ])
    return logging.getLogger(logger_name)
    '''
    logging.basicConfig(level=logging.DEBUG,
                       format=logging_format,
                       filename=logger_file_path,
                       filemode='w')
    
    output_logger = logging.getLogger(logger_name)
    # define a handler that outputs a rotating log file
    
    # define a Handler which writes messages to stdout
    console = logging.StreamHandler(sys.stdout)
    console_formatter = logging.Formatter(fmt = logging_format)
    console.setFormatter(console_formatter)
    console.setLevel(logging.DEBUG)
    output_logger.addHandler(console)
    # define a Handler which writes messages to stderr
    console_error = logging.StreamHandler(sys.stderr)
    console_error_formatter = logging.Formatter(fmt=logging_format)
    console_error.setFormatter(console_error_formatter)
    console_error.setLevel(logging.ERROR)
    output_logger.addHandler(console_error)
    return output_logger
    '''

def protien_count_reducing_function(accumulator, entry):
    indices_string_values = entry.split(' ') # converting the indices entry to indices values
    indices = list(map(lambda index_string: int(index_string), indices_string_values)) # converting the indices to in to use to increment the numpy accumulator
    accumulator[indices] += 1 # incrementing the accumulator counts
    return accumulator

def calculate_protien_statistics(dataFrame, logger, data_name, target_column_name, number_of_protiens):
    number_of_examples = dataFrame.shape[0]
    database_protien_positive_existance_sample_count = np.zeros(number_of_protiens, dtype=np.int)
    entries_to_be_reduced = [database_protien_positive_existance_sample_count]
    entries_to_be_reduced.extend(dataFrame[target_column_name])
    database_protien_positive_existance_sample_count = functools.reduce(protien_count_reducing_function,
                                                                        entries_to_be_reduced)
    logger.info('Protien counts in dataset {}: {}'.format(data_name,
        {idx: database_protien_positive_existance_sample_count[idx] for idx in range(number_of_protiens)}))
    database_protien_positive_existance_sample_ratio = database_protien_positive_existance_sample_count / float(
        number_of_examples)
    logger.info('Protien ratios in dataset {}: {}'.format(data_name,
        {idx: database_protien_positive_existance_sample_ratio[idx] for idx in range(number_of_protiens)}))
    return {'counts': database_protien_positive_existance_sample_count,
            'ratios': database_protien_positive_existance_sample_ratio}

def divide_dataset_random_policy(logger, datasets_name_ratio_dict = {'train': 0.9, 'validation': 0.05, 'test': 0.05},
                                 output_dataFrame_file_name='dividing_random_out.csv', random_seed = 7, dataset_root = '../Dataset'):
    logger.info('Openinig training csv file ...')
    all_dataFrame_file_name = 'train.csv'
    number_of_protiens = 28
    target_column_name = 'Target'
    all_dataFrame_file_path = os.path.join(dataset_root, all_dataFrame_file_name)
    all_dataFrame = pd.read_csv(all_dataFrame_file_path, sep=',', index_col=False)
    output_dataFrame = pd.DataFrame([], columns=['property', 'value'])
    # divide the data here
    logger.info('Dividing the dataset ...')
    #np.random.seed(random_seed)
    #np.random.shuffle(all_dataFrame) # will this work - it didnn't
    all_dataFrame = all_dataFrame.sample(frac=1, random_state=random_seed).reset_index(drop=True) # Shuffling and resetting the index
    datasets = {}
    start_index = 0
    end_index = 0
    all_dataset_length = all_dataFrame.shape[0]
    logger.info('All the data count: {}'.format(all_dataset_length))
    all_data_check_counter = 0
    for (entry_idx, (dataset_name, dataset_ratio)) in  enumerate(datasets_name_ratio_dict.items()):
        start_index = end_index
        end_index = start_index + int(dataset_ratio * all_dataset_length)
        if entry_idx == len(datasets_name_ratio_dict) - 1:
            end_index = all_dataset_length + 1
        datasets[dataset_name] = all_dataFrame.iloc[start_index:end_index]
        current_dataset_count = datasets[dataset_name].shape[0]
        logger.info('Current dataset: {} with count: {}'.format(dataset_name, current_dataset_count))
        logger.debug('Current dataset start_index')
        all_data_check_counter += current_dataset_count
        logger.info('Accumulated counts: {}'.format(all_data_check_counter))
    if all_data_check_counter != all_dataset_length:
        logger.error('all data length is: {} while the total all datasets length is {}'.format(all_dataset_length, all_data_check_counter))
    else:
        logger.info('Datasets sizes is ok')

    train_dataset_dataFrame = datasets['train']
    validation_dataset_dataFrame = datasets['validation']
    test_dataset_dataFrame = datasets['test']

    logger.info('Running some statistics ...')
    calculate_protien_statistics(all_dataFrame, logger, 'All', target_column_name, number_of_protiens)

    train_dataset_stat_dict = calculate_protien_statistics(train_dataset_dataFrame, logger, 'Training', target_column_name, number_of_protiens)
    database_protien_positive_existance_sample_ratio = train_dataset_stat_dict['ratios']

    calculate_protien_statistics(validation_dataset_dataFrame, logger, 'Validation', target_column_name, number_of_protiens)
    calculate_protien_statistics(test_dataset_dataFrame, logger, 'Test', target_column_name, number_of_protiens)

    # calculating the protien sampling weights that will be used for training
    training_weights = np.zeros(number_of_protiens * 2)
    for idx in range(training_weights.shape[0]):
        database_protien_positive_existance_sample_ratio_idx = int(idx/2)
        if idx%2 == 0:
            training_weights[idx] = database_protien_positive_existance_sample_ratio[database_protien_positive_existance_sample_ratio_idx]
        else:
            training_weights[idx] = 1 - database_protien_positive_existance_sample_ratio[
                database_protien_positive_existance_sample_ratio_idx]
    output_dataFrame.loc[output_dataFrame.shape[0]] = ['training_weights_for_classifier', training_weights]
    logger.info('Writting output the output of dividing the database ...')
    output_dataFrame_file_path = os.path.join(dataset_root, output_dataFrame_file_name)
    output_dataFrame.to_csv(output_dataFrame_file_path, header=1, index=False)
    logger.info('Finished diving the database with random strategy')



if __name__ == '__main__':
    main_logger = initializeLogger('dataset_divider', '../dataset_divider.log')
    #main_logger.info('Test info logger message')
    #main_logger.warning('Test warning logger message')
    #main_logger.error('Test error logger message')
    #main_logger.critical('Test critical logger message')
    divide_dataset_random_policy(main_logger)
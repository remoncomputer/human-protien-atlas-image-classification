# python imports
from enum import Enum
import random
import os

# pytorch imports
import torch
from torch.utils.data import Dataset

# other imports
import numpy as np
from PIL import Image
import pandas

class ProtienDataset(Dataset):
    colors = ['red','green','blue','yellow']
    train_folder_name = 'train'
    submission_folder_name = 'test'
    train_labels_file = 'train.csv'
    img_extension = 'png'

    train_dataset_ratio = 0.9
    validation_dataset_ratio = 0.05
    test_dataset_ratio = 1 - (train_dataset_ratio - validation_dataset_ratio)
    max_protien_idx = 27

    class datasetType(Enum):
        Train = 1
        Validation = 2
        Test = 3
        Submission = 4

    def __init__(self, rootDir, dataset_type = datasetType.Train, restricted_protien_idx = None, transform = None):
        self.rootDir = rootDir
        self.dataset_type = dataset_type
        self.transform = transform
        self.ids = []
        self.labels = []
        self.restricted_protien_idx = restricted_protien_idx
        if self.restricted_protien_idx > self.max_protien_idx:
            raise ValueError('Restricted Protien index can\'t be more than max_protien_idx')
        elif self.restricted_protien_idx < 0:
            raise ValueError('Restricted Protien index can\'t be smaller than zero')

        random.seed(7)
        torch.manual_seed(7)
        if self.dataset_type != self.datasetType.Submission:
            # Prepare the training dataset here
            self.folder_path = os.path.join(self.rootDir, self.train_folder_name)
            csvDataFrame = pandas.read_csv(os.path.join(self.rootDir, self.train_labels_file), header=0, index_col=False)
            self.ids = csvDataFrame['Id']
            stringTargets = csvDataFrame['Target']
            self.labels = map(self.convert_target_string_to_target_hot_vector, stringTargets)
            if self.restricted_protien_idx != None:
                (self.ids, self.labels)= self.filter_data_for_protien(self.ids, self.labels, self.restricted_protien_idx)
            data_len = len(self.ids)

            if self.dataset_type == self.datasetType.Train:
                start_idx = 0
                end_idx = np.floor(data_len * self.train_dataset_ratio) + 1
            elif self.dataset_type == self.datasetType.Validation:
                start_idx = np.floor(data_len * self.train_dataset_ratio) + 1
                end_idx = np.floor(data_len * (self.train_dataset_ratio + self.validation_dataset_ratio)) + 1
            elif self.dataset_type == self.datasetType.Test:
                start_idx = np.floor(data_len * (self.train_dataset_ratio + self.validation_dataset_ratio)) + 1
                end_idx = data_len
            else:
                raise ValueError('Undefined protien dataset type')

            self.ids = self.ids[start_idx:end_idx]
            self.labels = self.labels[start_idx:end_idx]
        else:
            # Prepare the submission dataset here
            self.folder_path = os.path.join(self.rootDir, self.submission_folder_name)
            file_names = os.listdir(self.folder_path)
            self.ids = set(map(lambda file_name: file_name.split('_')[0], file_names))

    def filter_data_for_protien(self, ids, labels, protien_idx):
        new_ids = []
        new_labels = []
        for (id, label) in zip(ids, labels):
            if label[protien_idx] == 1:
                new_ids.append(id)
                new_labels.append(1)
        return (new_ids, new_labels)


    def convert_target_string_to_target_hot_vector(self, targetString):
        outputTarget = torch.zeros(self.max_protien_idx + 1)
        targets = targetString.split(' ')
        for t in targets:
            t_int = int(t)
            outputTarget[t_int] = 1
        return outputTarget

    def loadImg(self, id):
        imgs = []
        for color in self.colors:
            img_name = id + '_' + color + '.' + self.img_extension
            img_path = os.path.join(self.folder_path, img_name)
            img_data = Image.open(img_path) #.convert('LA') # see if the loading will load it as grayscale or it will need to be converted
            imgs.append(img_data)
        final_img = np.hstack(imgs)
        return final_img

    def __getitem__(self, idx):
        id = self.ids[idx]
        img = self.loadImg(id)
        labels = None
        if self.dataset_type != self.datasetType.Submission:
            labels = self.labels[idx]#[self.output_protien_labels]
        return (id, img, labels)

    def __len__(self):
        return len(self.ids)